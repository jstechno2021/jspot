import React from 'react';
// eslint-disable-next-line prettier/prettier
import { NavigationContainer } from '@react-navigation/native';
// eslint-disable-next-line prettier/prettier
import { createStackNavigator } from '@react-navigation/stack';
import StartCall from './src/components/View/StartCall';
import Onboarding from './src/components/View/Onboarding.js';
import VideoCall from './src/components/View/VideoCall.js';
import Splash from './src/components/View/Splash.js';
import Selection from './src/components/View/Selection.js';

const MainStackNagivator = createStackNavigator();

const MainNavigator = () => {
  return (
    <MainStackNagivator.Navigator headerMode="none">
      <MainStackNagivator.Screen name="Splash" component={Splash} />
      <MainStackNagivator.Screen name="Selection" component={Selection} />
      <MainStackNagivator.Screen name="Onboarding" component={Onboarding} />
      <MainStackNagivator.Screen name="StartCall" component={StartCall} />
      <MainStackNagivator.Screen name="VideoCall" component={VideoCall} />
    </MainStackNagivator.Navigator>
  );
};

export default function App() {
  return (
    <NavigationContainer>
      <MainNavigator />
    </NavigationContainer>
  );
}
