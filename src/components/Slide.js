/* eslint-disable prettier/prettier */

import React from 'react';
import { StyleSheet, View, Dimensions, Image } from 'react-native';
const { width, height } = Dimensions.get('window');
export const SLIDER_HEIGHT = height;

const styles = StyleSheet.create({
  container: {
    width,
  },

  imageContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: "blue"
  },

  image: {
    width: undefined,
    height: undefined,
    backgroundColor: 'yellow',
  },
  imgs: {
    width: '100%',
    height: '100%',
    // alignItems: 'flex-start',
    // justifyContent: 'flex-start',
    // position: 'absolute',
    top: 0,
  },
});

const Slide = ({ img }) => {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image style={styles.imgs} source={img} />
      </View>
    </View>
  );
};

export default Slide;
