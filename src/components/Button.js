/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  container: {
    height: 50,
    borderRadius: 25,
    width: 245,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    fontSize: 15,
    fontWeight: '600',
  },
});
const Button = ({ variant, label, onPress }) => {
  const backgroundColor = variant === 'primary' ? '#125CA3' : 'white';
  const color = variant === 'primary' ? 'white' : '#0C0D34';

  return (
    <RectButton style={[styles.container, { backgroundColor }]} {...{ onPress }}>
      <Text style={[styles.label, { color }]}>{label}</Text>
    </RectButton>
  );
};

export default Button;
