import React from 'react';
import {StyleSheet} from 'react-native';
import Animated, {interpolate, Extrapolate} from 'react-native-reanimated';

const styles = StyleSheet.create({
  sliderIndicator: {
    backgroundColor: '#125CA3',
    height: 8,
    borderRadius: 4,
    margin: 4,
  },
});

const Dot = ({index, currentScrollIndex}) => {
  const opacity = interpolate(currentScrollIndex, {
    inputRange: [index - 1, index, index + 1],
    outputRange: [0.5, 1, 0.5],
    extrapolate: Extrapolate.CLAMP,
  });

  const scale = interpolate(currentScrollIndex, {
    inputRange: [index - 1, index, index + 1],
    outputRange: [1, 1.25, 1],
    extrapolate: Extrapolate.CLAMP,
  });

  const width = interpolate(currentScrollIndex, {
    inputRange: [index - 1, index, index + 1],
    outputRange: [8, 20, 8],
    extrapolate: Extrapolate.CLAMP,
  });

  return (
    <Animated.View
      style={[
        styles.sliderIndicator,
        {width, scale, opacity, transform: [{scale}]},
      ]}
    />
  );
};

export default Dot;
