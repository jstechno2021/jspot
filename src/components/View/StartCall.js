/* eslint-disable quotes */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  Dimensions,
  Keyboard,
  Animated,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  BackHandler,
} from 'react-native';
import Button from '../Button';
import { TextInput } from 'react-native-gesture-handler';

const BORDER_RADIUS = 40;
// const PRIMARY_COLOR = '#125CA3';
const PRIMARY_COLOR = '#233F82';
const API_URL = 'https://meet-admin.jighi.com';

const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: PRIMARY_COLOR,
  },

  // Start Form
  formHead: {
    // backgroundColor: PRIMARY_COLOR,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingHorizontal: 44,
    paddingVertical: height <= 736 ? 12 : 24,
  },

  ImageBg: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    position: 'absolute',
    alignSelf: 'center',
    elevation: 10,
    marginTop: 20,
  },
  formHeadTitleContainer: {
    borderBottomColor: 'rgba(255,255, 255, .5)',
    borderBottomWidth: 1,
    paddingBottom: 8,
    marginBottom: 8,
  },

  formHeadTitle: {
    color: 'white',
    fontSize: 32,
    fontWeight: '900',
  },

  formHeadDescription: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'normal',
  },

  formBody: {
    backgroundColor: 'transparent',
    // borderTopLeftRadius: BORDER_RADIUS,
    // borderTopRightRadius: BORDER_RADIUS,
    paddingHorizontal: 44,
    paddingVertical: 24,
  },

  formBodyTitle: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: '600',
  },

  formInput: {
    borderColor: '#B9B9B9',
    borderWidth: 2,
    borderRadius: 32,
    paddingHorizontal: 16,
    paddingVertical: 12,
    margin: 8,
    color: 'black',
  },

  // End Form

  text: {
    fontSize: 24,
  },

  // Start Modal
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },

  modalBody: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
  },

  modalTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 15,
    textAlign: 'center',
  },
  button: {
    fontSize: 15,
    fontWeight: '600',
  },

  modalText: {
    marginBottom: 24,
    textAlign: 'center',
  },
  TextStyle: {
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'black',
    fontFamily: undefined,
    fontSize: 24,
    fontStyle: 'normal',
    marginBottom: 10,
  },
  RectangleShape: {
    width: '80%',
    height: '70%',
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: '30%',
    elevation: 20,
  },
  // End Modal
});

const StartCall = ({ navigation }) => {
  useEffect(() => {
    const backAction = () => {
      navigation.navigate('Selection');
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );



    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);


    };
  }, []);


  const _keyboardDidShow = () => {
    console.log('-----------------------------');
    console.log('Keyboard Shown...');
    startAnimation(1);
  };

  const _keyboardDidHide = () => {
    console.log('-----------------------------');
    console.log('Keyboard Hidden...');
    startAnimation(0);
  };

  let [animation] = useState(new Animated.Value(0));

  const startAnimation = (keyboardVisible) => {
    Animated.timing(animation, {
      toValue: keyboardVisible ? 1 : 0,
      duration: 350,
      useNativeDriver: false,

    }).start();
  };

  const boxHeadInterpolate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: ['35%', '0%'],
  });

  const boxBodyInterpolate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: ['65%', '100%'],
  });

  const opacityInterpolate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 0],
  });

  const animatedStyle = {
    head: { height: boxHeadInterpolate },
    body: { height: boxBodyInterpolate },
    formTitleContainer: {
      opacity: opacityInterpolate,
    },
    formDescriptionContainer: {
      opacity: opacityInterpolate,
    },
  };

  const [modal, setModal] = useState({
    visible: false,
    title: '',
    message: '',
  });

  const [formData, setFormData] = useState({
    room_name: 'jighimeet1',
    username: 'guest',
    password: 'Guest@2020#',
    name: '',
  });

  const [sendData, setSendData] = useState({ isLoading: false });

  const getToken = async () => {
    if (
      formData.room_name === '' ||
      formData.username === '' ||
      formData.password === '' ||
      formData.name === ''
    ) {
      setModal({
        visible: true,
        title: 'Validation Error',
        message: 'Please try to fill correctly.',
      });
      return true;
    }

    //
    setSendData({ isLoading: true });

    await fetch(API_URL + '/api/token/get-token', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
      .then((response) => response.json())
      .then((data) => {
        setSendData({ isLoading: false });
        if (data.code === 400) {
          setModal({
            visible: true,
            title: 'Authentication Failed',
            message: data.error_message,
          });
          return true;
        }
        let token = data.data.token;
        navigation.navigate('VideoCall', { token, formData });
      })
      .catch((error) => {
        //
      });
  };

  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/img/backbg.png')} style={styles.ImageBg} />



      <View style={styles.RectangleShape} >

        {/* <KeyboardAvoidingView styles={styles.container} behavior="padding" keyboardShouldPersistTaps="always"> */}


        <Animated.View style={[styles.formHead, animatedStyle.head]}>
          <Animated.View
            style={[
              styles.formHeadTitleContainer,
              animatedStyle.formTitleContainer,
            ]}>
            {/* <Text style={styles.formHeadTitle}>
            {' '}
            Jighi Meet Video Conference{' '}
          </Text> */}
          </Animated.View>
          <Animated.View style={[animatedStyle.formDescriptionContainer]}>
            {/*  <Text style={styles.formHeadDescription}>
            {' '}
            Correctly fill in the fields below to join a meeting, for more
            information contact the administrator.
          </Text> */}
          </Animated.View>
        </Animated.View>

        {/* <Animated.View style={[styles.formBody, animatedStyle.body]}> */}



        <Animated.View style={[styles.ImageBg, animatedStyle.body]}>
          <View style={styles.TextStyle}>
            <Text style={styles.formBodyTitle}> Join Meeting </Text>
          </View>

          <View style={{ marginTop: 24 }}>
            <View>
              <TextInput
                style={styles.formInput}
                placeholder="Conference Room"
                autoCapitalize={'none'}
                defaultValue={formData.room_name}
                onChangeText={(text) =>
                  setFormData({ ...formData, room_name: text })
                }
              />
            </View>

            <View>
              <TextInput
                style={styles.formInput}
                placeholder="Username"
                autoCapitalize={'none'}
                defaultValue={formData.username}
                onChangeText={(text) =>
                  setFormData({ ...formData, username: text })
                }
              />
            </View>

            <View>
              <TextInput
                style={styles.formInput}
                placeholder="Password"
                autoCapitalize={'none'}
                defaultValue={formData.password}
                onChangeText={(text) =>
                  setFormData({ ...formData, password: text })
                }
              />
            </View>

            <View>
              <TextInput
                style={styles.formInput}
                placeholder="First & Last Names"
                autoCapitalize={'none'}
                defaultValue={formData.name}
                onChangeText={(text) => setFormData({ ...formData, name: text })}
              />
            </View>
            <View style={{ alignItems: 'center', marginTop: 24 }}>
              <Button
                label={sendData.isLoading === false ? 'Enter to join conference' : 'Loading ...'}
                variant="primary"
                onPress={
                  sendData.isLoading === false ? () => getToken() : () => false
                }
              />
            </View>
          </View>
        </Animated.View>

        <Modal
          transparent={false}
          visible={modal.visible}
          onRequestClose={() => {

            Alert.alert('Modal has been closed.');
          }}
          animationType="slide">
          <View style={styles.modalContainer}>
            <View style={styles.modalBody}>
              <Text style={styles.modalTitle}>{modal.title}</Text>
              <Text style={styles.modalText}>{modal.message}</Text>

              {/* <View style={styles.modalContainer}>
                <Text style={{ textAlign: 'center' }}> Splash Screen Example</Text>
              </View> */}


              {/* <Button
                label="Close"
                variant="primary"
                onPress={() => {
                  setModal({ visible: !modal.visible });
                }}
              /> */}

              <TouchableOpacity
                style={styles.backgroundImage}
                onPress={() => navigation.navigate('Onboarding')}>
                <Text style={styles.button}>Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        {/* </KeyboardAvoidingView> */}
      </View>
    </View>
  );
};

export default StartCall;
