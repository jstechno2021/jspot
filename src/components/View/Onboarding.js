/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useRef, useEffect } from 'react';
import { StyleSheet, View, Dimensions, BackHandler } from 'react-native';
import { useValue, onScrollEvent, interpolateColor } from 'react-native-redash';
import Animated, { multiply, divide } from 'react-native-reanimated';
import Slide, { SLIDER_HEIGHT } from '../Slide';
import SubSlide from '../SubSlide';
import Dot from '../Dot';

const { width } = Dimensions.get('window');

const slides = [
  {
    title: 'Relaxed',
    subtitle: 'Welcome !',
    description:
      "Jighi Meet's is a videoconferencing app, It strictly reserved for the staff of Jighi and its affiliates worldwide.",
    color: '#4D5AA8',
    img: require('../../assets/img/meeting_1.png'),
  },
  {
    title: 'Playfule',
    subtitle: 'How to join a meeting ?',
    description:
      'You must be invited by your host. he will provide you the needful information for joining a meeting.',
    color: '#233F82',
    img: require('../../assets/img/meeting_2.png'),
  },
  {
    title: 'Excentric',
    subtitle: "Let's go !",
    description:
      'Great, you have everything you need, it is with great pleasure that we wish you a pleasant meeting.',
    //color:"#4D5AA8",
    color: '#125CA3',
    img: require('../../assets/img/meeting_3.png'),
  },
];

const BORDER_RADUIS = 40;
const styles = StyleSheet.create({
  container: {
    height: SLIDER_HEIGHT,
    flex: 1,
    // position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: '#233F82',
    color: '#233F82',
    // justifyContent: 'center',
    // alignItems: 'center',
  },

  slider: {
    height: SLIDER_HEIGHT,
    backgroundColor: '#233F82',
    marginTop: 20,
    //borderBottomRightRadius: BORDER_RADUIS
  },

  footer: {
    flex: 1,
  },

  footerContent: {
    flex: 1,
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 30,
    // borderTopLeftRadius: BORDER_RADUIS,
    // borderTopRightRadius: BORDER_RADUIS,
  },

  pagination: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: BORDER_RADUIS,
    position: 'absolute',
    // marginLeft: '40%',
    bottom: 180,
    marginLeft: width * 0.4,
  },

  footerSlider: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    width: width * slides.length,
    // borderTopLeftRadius: BORDER_RADUIS,
  },
});

const Onboarding = ({ navigation }) => {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => true);
    return () =>
      BackHandler.removeEventListener('hardwareBackPress', () => true);
  }, []);

  const scroll = useRef(null);
  const x = useValue(0);
  const onScroll = onScrollEvent({ x });
  const backgroundColor = interpolateColor(x, {
    inputRange: slides.map((_, i) => i * width),
    outputRange: slides.map((slide) => slide.color),
  });

  return (
    <>
      <View style={styles.container}>
        <Animated.View style={[styles.slider, { backgroundColor }]}>
          <Animated.ScrollView
            ref={scroll}
            horizontal
            snapToInterval={width}
            decelerationRate="fast"
            showsHorizontalScrollIndicator={false}
            bounces={false}
            scrollEventThrottle={1}
            {...{ onScroll }}>
            {slides.map(({ img }, i) => (
              <Slide key={i} {...{ img }} right={i % 2} />
            ))}
          </Animated.ScrollView>
        </Animated.View>
        <View style={styles.footer}>
          <Animated.View
            style={{ ...StyleSheet.absoluteFillObject, backgroundColor }}
          />

          <Animated.View style={styles.footerContent}>
            <View style={styles.pagination}>
              {slides.map((_, index) => (
                <Dot
                  key={index}
                  currentScrollIndex={divide(x, width)}
                  {...{ index }}
                />
              ))}
            </View>
            <Animated.View
              style={[
                styles.footerSlider,
                { transform: [{ translateX: multiply(x, -1) }] },
              ]}>
              {slides.map(({ subtitle, description }, index) => (
                <SubSlide
                  key={index}
                  last={index === slides.length - 1}
                  {...{ subtitle, description, x }}
                  onPress={
                    index === slides.length - 1
                      ? () => {
                        // navigation.navigate('StartCall');
                        navigation.navigate('Selection');
                      }
                      : () => {
                        if (scroll.current) {
                          scroll.current.getNode().scrollTo({
                            x: width * (index + 1),
                            animated: true,
                          });
                        }
                      }
                  }
                />
              ))}
            </Animated.View>
          </Animated.View>
        </View>
      </View>
    </>
  );
};

export default Onboarding;
