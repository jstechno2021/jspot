/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react';
import JitsiMeet, { JitsiMeetView } from 'react-native-jitsi-meet';

const VideoCall = ({ navigation, route }) => {
  let { token, formData } = route.params;

  useEffect(() => {
    setTimeout(() => {
      const url = token.url;
      const userInfo = {
        displayName: formData.names,
        email: '',
        avatar: 'https:/gravatar.com/avatar/abc123',
      };
      JitsiMeet.call(url, userInfo);
    }, 1000);
    /*
    console.log("View Video Call mounted!");
    console.log({url: token.url, formData})
    */
  }, [formData.names, token.url]);

  useEffect(() => {
    return () => {
      JitsiMeet.endCall();
    };
  });

  function onConferenceTerminated(nativeEvent) {
    /* Conference terminated event */
    console.log(nativeEvent);
    navigation.navigate('StartCall');
  }

  function onConferenceJoined(nativeEvent) {
    /* Conference joined event */
    console.log(nativeEvent);
  }

  function onConferenceWillJoin(nativeEvent) {
    /* Conference will join event */
    console.log(nativeEvent);
  }

  return (
    <JitsiMeetView
      onConferenceTerminated={(e) => onConferenceTerminated(e)}
      onConferenceJoined={(e) => onConferenceJoined(e)}
      onConferenceWillJoin={(e) => onConferenceWillJoin(e)}
      style={{
        flex: 1,
        height: '100%',
        width: '100%',
      }}
    />
  );
};
export default VideoCall;
