/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */
// /* eslint-disable no-undef */
/* eslint-disable prettier/prettier */


/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */

/* eslint-disable quotes */
/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */


/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */


/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */
/* eslint-disable prettier/prettier */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
/* eslint-disable prettier/prettier */





/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */

import React, { useRef, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  BackHandler, Alert
} from 'react-native';
import Button from '../Button';
import Slide, { SLIDER_HEIGHT } from '../Slide';

const selectionImage = [

  {
    img: require('../../assets/img/meeting_3.png'),
  },
];

const styles = StyleSheet.create({

  containerColumn: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    color: '#ffffff'
  },
  containerRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  containerRowTextJighi: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  containerRowTextCI: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginLeft: 50
  },

  CircleJighi: {
    width: 130,
    height: 130,
    borderRadius: 150 / 2,
    backgroundColor: '#00BCD4',
    borderColor: 'black',
    margin: 8,
  },
  CircleCIVideo: {
    width: 130,
    height: 130,
    borderRadius: 150 / 2,
    backgroundColor: '#00BCD4',
    margin: 8,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
    borderRadius: 150 / 2,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: '#233F82',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  backgroundImageNew: {
    flex: 1,
    resizeMode: 'cover',
  },
  TextStyle: {
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'black',
    fontFamily: undefined,
    fontSize: 24,
    fontStyle: 'normal',
    marginBottom: 10
  }
  ,
  TextStyleNew: {
    fontWeight: 'normal',
    textAlign: 'center',
    color: 'black',
    fontFamily: undefined,
    fontSize: 20,
    fontStyle: 'normal',
    marginBottom: 10,
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
  },
  containerMain:
  {
    flex: 1,
    backgroundColor: '#233F82',
    color: '#233F82'
  },
  RectangleShape: {
    width: '80%',
    height: '65%',
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: '30%',
    elevation: 10,
  },
  buttonShape: {
    marginTop: 10,
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 30,
    marginRight: 30,
    backgroundColor: '#00BCD4',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    justifyContent: "center",
    alignItems: "center",
  },
  newBg: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
  },
  ImageBg: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: '100%',
    height: '100%',
    position: 'absolute',
    marginTop: 20,
  },
  imgs: {
    alignItems: 'flex-start',

  }

});



const Selection = ({ navigation }) => {
  useEffect(() => {
    const backAction = () => {
      navigation.navigate('Onboarding');
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    // return () => backHandler.remove();
    return () => navigation.navigate('Onboarding');
  }, []);

  const scroll = useRef(null);

  // setTimeout(function () { navigation.navigate('Onboarding'); }, 2000);

  return (
    <>
      <View style={styles.containerMain}>
        <Image
          source={require('../../assets/img/backbg.png')} style={styles.ImageBg} />

        <View style={styles.RectangleShape} >

          <View style={styles.containerColumn}>
            <View >
              <Text style={styles.TextStyle}> Platform</Text>
            </View>
            <View >
              <Text style={styles.TextStyleNew}> Select meeting Platform</Text>
            </View>



            <View style={styles.containerRow}>

              <View style={styles.CircleJighi}>
                <TouchableOpacity
                  style={styles.backgroundImage}
                  onPress={() => navigation.navigate('StartCall')}>
                  <Image
                    style={styles.imgs}
                    source={require('../../assets/img/jighi_meet.png')} />
                </TouchableOpacity>
              </View>

              <View style={styles.CircleCIVideo}>
                <TouchableOpacity
                  style={styles.backgroundImage}
                  onPress={() => navigation.navigate('StartCall')}>
                  <Image
                    source={require('../../assets/img/civideo.png')} />
                </TouchableOpacity>

              </View>

            </View>

            <View style={styles.containerRowTextJighi}>
              <View >
                <Text style={styles.TextStyleNew}> Jighi Meet</Text>
              </View>

              <View style={styles.containerRowTextCI}>
                <Text style={styles.TextStyleNew}> CI Video</Text>
              </View>
            </View>
          </View>
        </View>
      </View>

    </>
  );
};

export default Selection;
