/* eslint-disable prettier/prettier */
/* eslint-disable prettier/prettier */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
/* eslint-disable prettier/prettier */





/* eslint-disable prettier/prettier */

/* eslint-disable prettier/prettier */

import React, { useRef } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Button,
  Text
} from 'react-native';



const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  footer: {
    flex: 1,
  },

  footerContent: {
    flex: 1,
    backgroundColor: 'white',
  },
  formHeadTitle: {
    color: 'black',
    fontSize: 32,
    fontWeight: '900',
  },
  pagination: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  SplashScreen_RootView:
  {
    justifyContent: 'center',
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
  },

  SplashScreen_ChildView:
  {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#233F82',
    flex: 1
  },
  backgroundImage: {
    flex: 1,
    width: '100%',
    height: '100%',
    resizeMode: 'cover'
  }
});



const Splash = ({ navigation }) => {
  const scroll = useRef(null);

  setTimeout(function () { navigation.navigate('Onboarding'); }, 2000);

  return (
    <>
      <View style={styles.SplashScreen_RootView}>

        <View style={styles.SplashScreen_ChildView}>
          {/* <Image
            source={{
              uri:
                'https://static.javatpoint.com/tutorial/react-native/images/react-native-tutorial.png',
            }}
            style={{ width: '100%', height: '100%', resizeMode: 'contain' }}
          /> */}
          <Image
            style={styles.backgroundImage}
            source={require('../../assets/img/new_splash.png')} />
        </View>
      </View>
    </>
  );
};

export default Splash;
